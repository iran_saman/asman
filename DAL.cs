﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace WindowsFormsApplication3
{
    class DAL
    {
        SqlConnection con;
        SqlCommand com;
        SqlDataAdapter da;
        public DAL()
        {

            con = new SqlConnection();
            com = new SqlCommand();
            da = new SqlDataAdapter();
            com.Connection = con;
            da.SelectCommand = com;
        }

        public void conect()
        {
            con.ConnectionString = @" Data Source=ANA-PC\SQLEXPRESS;Initial Catalog=std;Integrated Security=True;Pooling=False  ";
            con.Open();

        }
        public void disconect()
        {
            con.Close();

        }
        public void EXE(string SQL)
        {
            com.CommandText = SQL;
            com.ExecuteNonQuery();



        }
        public DataTable Select(string SQL)
        {
            com.CommandText = SQL;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }
    }
}
